<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreQuotationRequest;
use App\Http\Requests\UpdateQuotationRequest;
use App\Models\Quotation;
use Illuminate\Contracts\Foundation\Application;

class QuotationController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show()
    {
        $test = Quotation::all();
        return view('quotation.all', [
            'quotation' => $test,
        ]);
    }

}
