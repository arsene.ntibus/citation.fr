TODO

1- Model de Donnée

-Homepage: listant la dernière citation et son auteur (au clic sur l'auteur, j'accède à la page des citations de l'auteur)


2-CRUD Auteur  
    -CREATE AUTEUR 
    -READ AUTEUR one to many 
    -UPDATE AUTEUR
    -DELETE AUTEUR

3-CRUD CITATION
    -CREATE CITATION X
    -READ CITATION one and all
    -UPDATE CITATION
    -DELETE CITATION

-Les citations : listant toutes les citations avec une pagination (10 citations par page)
-Les auteurs: listant tous les auteurs ayant des citations.
-Les citation d'un auteur: listant toutes les citations pour un auteur donné
