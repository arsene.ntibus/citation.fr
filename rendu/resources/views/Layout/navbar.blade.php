<header>

    @yield("navbar)<h1 class="logo"><img src="/assets/Img/Citation.png" alt="" class="citationlogo" srcset=""></h1>
    <input type="checkbox" id="nav-toggle" class="nav-toggle">
    <nav>
        <ul>
            <li><a href="/" class="active">HomePage</a></li>
            <li><a href="#">Les Citations</a></li>
            <li><a href="#">Les Auteurs</a></li>
            <li><a href="#">Les Citation et leurs Auteurs </a></li>
        </ul>
    </nav>
    <label for="nav-toggle" class="nav-toggle-label">
        <span></span>
    </label>
</header>
