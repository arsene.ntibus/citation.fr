<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{'css/app.css'}}">
    {{ $title ?? 'Citation.fr' }}
</head>
<header>


    <h1 class="logo"><img src="/assets/Img/Citation.png" alt="" class="citationlogo" srcset=""></h1>
    <input type="checkbox" id="nav-toggle" class="nav-toggle">
    <nav>
        <ul>
            <li><a href="/" class="active">HomePage</a></li>
            <li><a href="#">Les Citations</a></li>
            <li><a href="#">Les Auteurs</a></li>
            <li><a href="#">Les Citations et leurs Auteurs </a></li>
        </ul>
    </nav>
    <label for="nav-toggle" class="nav-toggle-label">
        <span></span>
    </label>
</header>
<!-- Contenue  de La  Page  (Bouton qui redirige vers citation ) -->
<div class="content">

    <div class="border">
        <h1 class="title"> Bienvenue Sur Citation.Fr </h1>
    </div>



</div>

{{$slot}}
<button class="button">
    <h3>
        <a href="#">La Dernière Citation </a>
    </h3>
</button>

<footer>
    <p>@citation.fr</p>
</footer>

</html>
