<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{"app/style"}}">
    <title>{{ $title ?? 'citation.fr' }} </title>
</head>
<header>
    <h1 class="logo"><img src="/assets/Img/Citation.png" alt="Citation.frlogo" class="citationlogo"></h1>
    <input type="checkbox" id="nav-toggle" class="nav-toggle">
   <nav>
        <ul>
            <li><a href="#">HomePage</a></li>
            <li><a href="#">Les Citations</a></li>
            <li><a href="#">Les Auteurs</a></li>
            <li><a href="#">Les Citations et leurs Auteurs.</a></li>
        </ul>
    </nav>
    <label for="nav-toggle" class="nav-toggle-label">
        <span></span>
    </label>
</header>

<div class="content">

    <div class="border">
        <h1 class="title"> Bienvenue Sur Citation.Fr </h1>
    </div>
</div>
<div class="button">
    <h3>
        <a href="#">La Dernière Citation </a>
    </h3>
</div>
<!-- Fin  Main Content -->
<footer>
    <p>@citation.fr</p>
</footer>
</html>
